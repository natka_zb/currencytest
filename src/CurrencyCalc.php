<?php

namespace natka_zb\Currency;

class CurrencyCalc
{
    const URL_SOURCE_CBR = 'http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1={DATE}&date_req2={DATE}&VAL_NM_RQ={CURRENCY}';
    const URL_SOURCE_RBC = 'https://cash.rbc.ru/cash/json/converter_currency_rate/?currency_from={CURRENCY}&currency_to=RUR&source=cbrf&sum=1&date={DATE}';

    const R01235 = 'R01235'; // Доллар США
    const R01239 = 'R01239'; // Евро

    const EUR = 'EUR';
    const USD = 'USD';

    /*
     * @param string $date - format YYYY-MM-DD
     * @return array
     * @throws \Exception
     */
    public function getAvg($date)
    {
        $dateArray = explode('-', $date);
        try {
            if (!checkdate($dateArray[1], $dateArray[2], $dateArray[0])) {
                throw new \Exception('');
            }
        }
        catch (\Exception $e) {
            throw new \Exception('Incorrect input date');
        }
        $dateDDMMYYY = "$dateArray[2]/$dateArray[1]/$dateArray[0]";
        $usd = [
            $this->getCBR($dateDDMMYYY, static::R01235),
            $this->getRBC($date, static::USD)
        ];
        $eur = [
            $this->getCBR($dateDDMMYYY, static::R01239),
            $this->getRBC($date, static::EUR)
        ];
        return [
            static::USD => (array_sum($usd) / count($usd)),
            static::EUR => (array_sum($eur) / count($eur))
        ];
    }

    /*
     * @param string $date - format DD/MM/YYYY
     * @param string $currency
     * @return float
     * @throws \Exception
     */
    protected function getCBR($date, $currency)
    {
        $url = str_replace(['{DATE}', '{CURRENCY}'], [$date, $currency], static::URL_SOURCE_CBR);
        try {
            $result = file_get_contents($url);
        }
        catch (\Exception $e) {
            throw new \Exception('No response URL');
        }

        try {
            $xml = simplexml_load_string($result);
            $value = str_replace(',', '.', $xml->Record->Value);
            return (float)$value;
        }
        catch (\Exception $e) {
            throw new \Exception('Parse format error');
        }
    }

    /*
     * @param string $date - format YYYY-MM-DD
     * @param string $currency
     * @return float
     * @throws \Exception
     */
    protected function getRBC($date, $currency)
    {
        $url = str_replace(['{DATE}', '{CURRENCY}'], [$date, $currency], static::URL_SOURCE_RBC);
        try {
            $result = file_get_contents($url);
        }
        catch (\Exception $e) {
            throw new \Exception('No response URL');
        }

        try {
            $data = json_decode($result, true);
            $value = str_replace(',', '.', $data['data']['rate1']);
            return (float)$value;
        }
        catch (\Exception $e) {
            throw new \Exception('Parse format error');
        }
    }

}