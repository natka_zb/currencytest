# natka_zb/currencytest
Test currencies...

composer
-------------
* добавить в секцию require
```
"natka_zb/currencytest": "dev-master"
```
* добавить в секцию repositories
```
{
    "type": "git",
    "url": "git@bitbucket.org:natka_zb/currencytest.git"
}
```

использовать в коде
-------------
```
use natka_zb\Currency\CurrencyCalc;
```

запускать тесты
-------------
```
vendor/codeception/codeception/codecept run unit
```