<?php
use Codeception\Util\Fixtures;
use natka_zb\Currency\CurrencyCalc;
use Tests\Unit\CurrencyCalcUnit;

class CurrencyCest
{
    public function _before(UnitTester $I)
    {
    }

    public function _after(UnitTester $I)
    {
    }

    public function currencyTest(UnitTester $I)
    {
        $cc = new CurrencyCalc();
        if (Fixtures::exists('currencies_equals')) {
            $currencyData = Fixtures::get('currencies_equals');
            $test = $cc->getAvg($currencyData['date']);
            $I->assertEquals($currencyData['result'], $test, 'error: must be equals');
        }

        if (Fixtures::exists('currencies_no_equals')) {
            $currencyData = Fixtures::get('currencies_no_equals');
            $test = $cc->getAvg($currencyData['date']);
            $I->assertNotEquals($currencyData['result'], $test, 'error: must be no equals');
        }
    }

    public function currencyTestExceptionURL(UnitTester $I)
    {
        $cc = new CurrencyCalcUnit();
        if (Fixtures::exists('currencies_equals')) {
            $currencyData = Fixtures::get('currencies_equals');
            $I->expectException(new \Exception('No response URL'), function() use($cc, $currencyData) {
                $test = $cc->getAvg($currencyData['date']);
            });
        }
    }

    public function currencyTestExceptionDate(UnitTester $I)
    {
        $cc = new CurrencyCalc();
        if (Fixtures::exists('currencies_incorrect_data')) {
            $currencyData = Fixtures::get('currencies_incorrect_data');
            $I->expectException(new \Exception('Incorrect input date'), function() use($cc, $currencyData) {
                $test = $cc->getAvg($currencyData['date']);
            });
        }
    }
}
