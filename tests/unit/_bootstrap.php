<?php
require(__DIR__ . '/../../src/CurrencyCalc.php');
require(__DIR__ . '/CurrencyCalcUnit.php');
define("FIXTURES_DIR", __DIR__ . '/_fixtures/');
use Codeception\Util\Fixtures;
Fixtures::add('currencies_equals', require(FIXTURES_DIR . 'currencies_equals.php'));
Fixtures::add('currencies_no_equals', require(FIXTURES_DIR . 'currencies_no_equals.php'));
Fixtures::add('currencies_incorrect_data', require(FIXTURES_DIR . 'currencies_incorrect_data.php'));