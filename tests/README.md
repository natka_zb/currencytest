# Тестирование

КОНФИГУРАЦИЯ
-------------
* Добавляем в composer.json в секцию require-dev свежую версию Codeception
```
"codeception/codeception": "^2.4.1",
```
* Устанавливаем библиотеки Codeception
```
composer update codeception/codeception
```

ДОКУМЕНТАЦИЯ
-------------

https://codeception.com/ - Официальная

http://allframeworks.ru/codeception - Неофициальный перевод

http://devacademy.ru/posts/tiestirovaniiedah-api-ustanovka-i-ispol-zovaniie-codeception/# - тестирование API

https://klisl.com/codeception_db.html - модуль Db, фикстуры                                                             

ИСПОЛЬЗОВАНИЕ
-------------

* Сгенерировать тип тестирования API
```
codecept generate:suite api
```

* Построить тесты (каждый раз, когда меняется *.suite.yml или codeception.yml)
```
codecept build
```

* Запускать все тесты
```
codecept run
```

* Запускать все тесты с детализацией
```
codecept run --steps
```

* Запускать API тесты
```
codecept run api
```

* Запускать конкретный тест
```
codecept run tests/api/HotelCest.php:createHotelMutationTest
```

* Генерация теста 
```
codecept generate:cest api NameOfTest
```

ФИКСТУРЫ (наборы данных для тестов) для API
-------------

* Лежат тут: api/_fixtures
 ```
api/_fixtures/
```

* В api/_bootstrap.php добавляем
```
define("FIXTURES_DIR", __DIR__ . '/_fixtures/');
use Codeception\Util\Fixtures;
Fixtures::add('hotels', require(FIXTURES_DIR . 'hotels.php'));
```

* В тесте используем фикстуры так
```
use Codeception\Util\Fixtures;
if (Fixtures::exists('hotels')) {
    ...
    Fixtures::get('hotels');
    ...
}
```